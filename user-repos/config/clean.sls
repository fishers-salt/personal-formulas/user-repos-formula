# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as user__repos with context %}

{% if salt['pillar.get']('user-repos-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_user-repos', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

{% set code_directory = user.get('code_directory', 'code') %}
{%- set repos = user.get('repos', {}) %}
{% for directory, repo in repos.items() %}
user-repos-clean-repos-code-{{ directory }}-directory-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/{{ code_directory }}/{{ directory }}
{% endfor %}

user-repos-clean-repos-code-directory-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/{{ code_directory }}
{% endif %}
{% endfor %}
{% endif %}
