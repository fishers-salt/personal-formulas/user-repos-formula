# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as user_repos with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('user-repos-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_user-repos', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

user-repos-config-repos-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

{% set code_directory = user.get('code_directory', 'code') %}
user-repos-config-repos-code-directory-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/{{ code_directory }}
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - recurse:
      - user
      - group
    - require:
      - user-repos-config-repos-user-{{ name }}-present

{%- set repos = user.get('repos', {}) %}
{% for directory, repo in repos.items() %}
user-repos-config-repos-code-{{ directory }}-directory-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/{{ code_directory }}/{{ directory }}
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - require:
      - user-repos-config-repos-code-directory-{{ name }}-managed

{% for repo_name, remotes in repo.items() %}
user-repos-config-repos-code-{{ directory }}-{{ repo_name|replace('_', '-') }}-{{ name }}-cloned:
  git.latest:
    {%- set origin = remotes['origin'] %}
    - name: {{ origin }}
    - target: {{ home }}/{{ code_directory }}/{{ directory }}/{{ repo_name }}
    - user: {{ name }}
    {%- if origin.startswith('git') %}
    - identity:
      - salt://ssh/files/deploy_key
    {%- endif %}
    - require:
      - user-repos-config-repos-code-{{ directory }}-directory-{{ name }}-managed

{% for remote, address in remotes.items() %}
{%- if remote == 'origin' %}
{# Do nothing, already cloned the origin #}
{%- else %}
user-repos-config-repos-code-{{ directory }}-{{ repo_name|replace('_', '-') }}-{{ name }}-{{ remote }}-added:
  cmd.run:
    - name: 'git remote add {{ remote }} {{ address }}'
    - cwd: {{ home }}/{{ code_directory }}/{{ directory }}/{{ repo_name }}
    - runas: {{ name }}
    - require:
      - user-repos-config-repos-code-{{ directory }}-{{ repo_name|replace('_', '-') }}-{{ name }}-cloned
    - unless:
      - grep 'remote "{{ remote }}"' {{ home }}/{{ code_directory }}/{{ directory }}/{{ repo_name }}/.git/config
{% endif %}
{% endfor %}

user-repos-config-repos-code-{{ directory }}-{{ repo_name|replace('_', '-') }}-{{ name }}-group-managed:
  file.directory:
    - name: {{ home }}/{{ code_directory }}/{{ directory }}/{{ repo_name }}
    - group: {{ user_group }}
    - recurse:
      - group

{% endfor %}
{% endfor %}

{% endif %}
{% endfor %}
{% endif %}
