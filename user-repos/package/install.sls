# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as user_repos with context %}

user-repos-package-install-git-pkg-installed:
  pkg.installed:
    - name: {{ user_repos.gitpkg.name }}
