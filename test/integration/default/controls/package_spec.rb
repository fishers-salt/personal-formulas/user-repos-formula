# frozen_string_literal: true

control 'user-repos-package-install-git-pkg-installed' do
  title 'The required package should be installed'

  describe package('git') do
    it { should be_installed }
  end
end
