# frozen_string_literal: true

control 'user-repos-config-repos-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'user-repos-config-repos-code-directory-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/programming') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

code_dirs = %w[salt docs]
code_dirs.each do |dir|
  control "user-repos-config-repos-code-#{dir}-directory-auser-managed" do
    title 'should exist'

    describe directory("/home/auser/programming/#{dir}") do
      it { should be_owned_by 'auser' }
      it { should be_grouped_into 'auser' }
      its('mode') { should cmp '0755' }
    end
  end
end

control 'user-repos-config-repos-code-salt-template-formula-auser-cloned' do
  title 'should be cloned'

  describe directory('/home/auser/programming/salt/template-formula') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
  describe directory('/home/auser/programming/salt/template-formula/.git') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'user-repos-config-repos-code-salt-'\
        'template-formula-auser-gitlab-added' do
  describe command(
    'grep --max-count=1 gitlab '\
    '/home/auser/programming/salt/template-formula/.git/config'
  ) do
    its('stdout') { should eq "[remote \"gitlab\"]\n" }
    its('stderr') { should eq '' }
    its('exit_status') { should eq 0 }
  end
end

control 'user-repos-config-salt-external-formulas-directory-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/programming/salt/external-formulas') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'user-repos-config-repos-code-salt-external-formula-auser-cloned' do
  title 'should be cloned'

  describe directory(
    '/home/auser/programming/salt/external-formulas/foo-formula'
  ) do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
  describe directory(
    '/home/auser/programming/salt/external-formulas/foo-formula/.git'
  ) do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'user-repos-config-repos-code-salt-foo-'\
        'formulas-template-formula-auser-gitlab-added' do
  describe command(
    'grep --max-count=1 gitlab '\
    '/home/auser/programming/salt/external-formulas'\
    '/foo-formula/.git/config'
  ) do
    its('stdout') { should eq "[remote \"gitlab\"]\n" }
    its('stderr') { should eq '' }
    its('exit_status') { should eq 0 }
  end
end

control 'user-repos-config-repos-code-docs-mydocs-auser-cloned' do
  title 'should be cloned'

  describe directory('/home/auser/programming/docs/mydocs') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
  describe directory('/home/auser/programming/docs/mydocs/.git') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end
