# frozen_string_literal: true

control 'user-repos-clean-repos-code-docs-mydocs-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/programming/docs/mydocs') do
    it { should_not exist }
  end
end

control 'user-repos-clean-repos-code-salt-template-formula-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/programming/salt/template-formula') do
    it { should_not exist }
  end
end

code_dirs = %w[salt docs]
code_dirs.each do |dir|
  control "user-repos-clean-repos-code-#{dir}-directory-auser-absent" do
    title 'should not exist'

    describe directory("/home/auser/programming/#{dir}") do
      it { should_not exist }
    end
  end
end

control 'user-repos-clean-repos-code-directory-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/programming') do
    it { should_not exist }
  end
end
